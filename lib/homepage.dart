import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'file:///C:/Users/shafe/AndroidStudioProjects/trynew/lib/Bottom%20nav%20pages/addrest.dart';
import 'Bottom nav pages/first.dart';
import 'Bottom nav pages/settings.dart';
// ignore: camel_case_types
class homepage extends StatefulWidget {

  @override
  _homepageState createState() => _homepageState();
}
// ignore: camel_case_types
class _homepageState extends State<homepage> {
  int _currentindex =0;
  var _pages= [Home(), MyApp(), settings()];
  var _pageController = PageController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: PageView(
        children: _pages,
        onPageChanged: (index){
          setState(() {
            _currentindex = index;
          });
        },
        controller: _pageController,
      ),

      bottomNavigationBar: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(topLeft: Radius.circular(15), topRight: Radius.circular(15)),
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.only(topRight: Radius.circular(15.0), topLeft: Radius.circular(15)),
          child: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            backgroundColor: Colors.blue,
            selectedItemColor: Colors.white,
            currentIndex: _currentindex,
            items: [
              BottomNavigationBarItem(
                icon: Icon(Icons.home_outlined),
                // ignore: deprecated_member_use
                title: Text('home'),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.add_circle_outline),
                // ignore: deprecated_member_use
                title: Text('Restaurants'),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.settings),
                // ignore: deprecated_member_use
                title: Text('Settings'),
              ),
            ],
            onTap: (index){
              setState(() {
                _currentindex =index;
                _pageController.animateToPage
                  (_currentindex, duration: Duration(milliseconds: 200), curve: Curves.linear);
              });
            },
          ),
        ),
      ),
    );
  }
}
