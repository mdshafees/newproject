
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'homepage.dart';




void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: MyNew(),
  ));

}

class MyNew extends StatefulWidget {
  @override
  _MyNewState createState() => _MyNewState();
}

class _MyNewState extends State<MyNew> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: SplashScreen(
        seconds: 5,
        backgroundColor: Colors.black,
        image: Image.asset('assets/new123.jpg'),
        loaderColor: Colors.white,
        photoSize: 150.0,
        navigateAfterSeconds: MyApp(),
      ),
    );
  }
}
/////////////////////////////////////
class MyApp extends StatefulWidget {


  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String email , password;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();





  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all(25.0),
        child: Center(
          child: Form(
            // ignore: deprecated_member_use
            key: formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  child: ClipOval(
                    child: Align(
                        widthFactor: 0.3,
                        heightFactor: 0.5,
                        child: Image.asset('assets/new123.jpg')),
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: TextFormField(
                    // ignore: missing_return
                    validator: (input) {
                      if(input.isEmpty){
                        return 'Please type an Email';
                      }
                    },
                    onSaved: (input) => email = input,
                    controller: emailController,
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: "Email",
                      suffixIcon: Icon(Icons.mail_outline),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: TextFormField(
                    // ignore: missing_return
                    validator: (input) {
                      if(input.length <6){
                        return 'atleast need 6 chracters';
                      }
                    },
                    onSaved: (input) => password = input,
                    controller: passwordController,
                    obscureText: true,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: "Password",
                      suffixIcon: Icon(Icons.lock_outline),

                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: RaisedButton(
                    onPressed:() {
                      signIn();
                    },

                    child: Text('Sign in'),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
  Future<void> signIn() async {
    final formState =formKey.currentState;
    if (formState.validate()){
      formState.save();
      try{
//noname@gmail.com //aforapple
        UserCredential user = await FirebaseAuth.instance.signInWithEmailAndPassword(email: email, password: password);
        Navigator.push(context, MaterialPageRoute(builder: (context)=>homepage()));
      }catch(e){
        print(e.message);
      }

    }
  }
}
