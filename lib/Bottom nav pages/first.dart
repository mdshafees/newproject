import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../update page.dart';

class Home extends StatefulWidget {
  var name, num, est, loc;

  Home({@required this.name, this.num, this.est, this.loc});

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  CollectionReference ref = Firestore.instance.collection("hotels lists");


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('List of Hotels'),
        centerTitle: true,
      ),
      body: StreamBuilder(
          stream: ref.snapshots(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) return Text('Loading....');
            return new ListView(
                children: snapshot.data.documents
                    .map<Widget>((DocumentSnapshot document) {
                  return new CustomCard(
                    name: document['name'],
                    number: document['number'],
                    date: document['date'],
                    location: document['location'],
                  );
                }).toList());
          }),
    );
  }
}

class CustomCard extends StatelessWidget {
  CustomCard({@required this.name, this.number, this.location, this.date});

  final name;
  final number;
  final location;
  final date;
  TextEditingController nameController = new TextEditingController();
  TextEditingController numberController = new TextEditingController();
  TextEditingController dateController = new TextEditingController();
  TextEditingController locationController = new TextEditingController();

  get doc => null;

  @override
  Widget build(BuildContext context) {
    return Card(
        child: Container(
            padding: const EdgeInsets.only(top: 5.0),
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: [
                      Text(
                        'Hotel: ',
                        style: TextStyle(
                          color: Colors.green,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(
                        width: 145.0,
                      ),
                      Text(name),
                    ],
                  ),
                  Row(
                    children: [
                      Text(
                        'Call: ',
                        style: TextStyle(
                          color: Colors.green,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(
                        width: 155.0,
                      ),
                      Text(number),
                    ],
                  ),
                  Row(
                    children: [
                      Text(
                        'Date of Establishment: ',
                        style: TextStyle(
                          color: Colors.green,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(
                        width: 40.0,
                      ),
                      Text(date),
                    ],
                  ),
                  Row(
                    children: [
                      Text(
                        'Location: ',
                        style: TextStyle(
                          color: Colors.green,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(
                        width: 125.0,
                      ),
                      Text(location),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        FlatButton(
                            color: Colors.blue,
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => update()));
                            },
                            child: Text(
                              'Update',
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            )),
                        SizedBox(
                          width: 20.0,
                        ),
                        FlatButton(
                            color: Colors.red,
                            onPressed: () {
                              Firestore.instance
                                  .collection('hotels lists')
                                  .document().delete();
                              },
                            child: Text(
                              'Delete',
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            )),
                      ],
                    ),
                  ),
                ],
              ),
            )));
  }

  deleteData(docId) {
    Firestore.instance
        .collection('hotels lists')
        .document(docId)
        .delete()
        .catchError((e) => {print(e)});
  }
}
